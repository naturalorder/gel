var budgetController = angular.module('budgetController',[
	'ngCookies',
	'ui.bootstrap'
]);

budgetController.controller('homeController', ['$scope', '$http', '$timeout',
	function($scope,$http,$timeout){
		$scope.tasks = [];

    (function poll(){
      $http.get('/tasks').
        success(function(data, status, headers, config) {
            console.log(data);
            $scope.tasks = data;
        }).
        error(function(data, status, headers, config) {

        });
      $timeout(poll, 1000);
    })();

	}
]);

budgetController.controller('ModalDemoCtrl', ['$rootScope', '$modal', '$log',
	function($rootScope, $modal, $log){
		$rootScope.open = function () {

		    var modalInstance = $modal.open({
		      templateUrl: 'signupModal',
		      controller: 'ModalInstanceCtrl',
		      size: 'sm'

		    });

		    modalInstance.result.then(function () {
		      // $rootScope.selected = selectedItem;
		    }, function () {
		      $log.info('Modal dismissed at: ' + new Date());
		    });
	  };
	}
]);

budgetController.controller('ModalInstanceCtrl', ['$rootScope', '$modalInstance',
	function ($rootScope, $modalInstance) {

}]);
