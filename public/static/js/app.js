var budgetApp = angular.module('budgetApp', [
	'budgetController',
	'ngResource'

]);

budgetApp.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
});

budgetApp.factory('ajaxInterceptor',['$q', '$rootScope', '$injector', 
	function($q, $rootScope, $injector){
		return {
			responseError: function(response){
				console.log("It was here!");
				if (response.status === 401){
	            	//console.log($scope);
					$rootScope.open();
					console.log("You are not logged in!");
      			}
      			return $q.reject(response);
			} 
		};
}]);


budgetApp.config(['$httpProvider', function($httpProvider){
	$httpProvider.interceptors.push('ajaxInterceptor');
}]);